import React, { Component } from 'react';
import './App.css';
import Menu from './Components/Menu';
import Login from './Components/Login';
import Home from './Components/Home';
import PrivateRoutes from './Components/PrivateComponent'
import Signup from './Components/Signup';
import Grenner from './Components/Grenner';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import FoodMap from './Components/FoodMap';
import Profile from './Components/Profile';
import Recipe from './Components/Recipe'



class App extends Component {

  render() {
    return (
      <div className="App">
        <Menu />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/Menu' component={Menu} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/signup' component={Signup} />
          <PrivateRoutes exact path='/grenner' component={Grenner} />
          <PrivateRoutes exact path='/foodmap' component={FoodMap} />
          <PrivateRoutes exact path='/profile' component={Profile} />
          <PrivateRoutes exact path='/recipe' component={Recipe}/>
        </Switch>
      </div>
    );
  }
}

export default App;
