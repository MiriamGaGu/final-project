import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
const API_URL = 'https://thawing-ocean-76607.herokuapp.com/api/v1';


class Profile extends Component {
    constructor() {
        super()
        this.state = {
            user: '',
            email:'',
            error: {
                status: false,
                message: ''
            }
        }
    }

    componentDidMount() {
        fetch(`${API_URL}/users`, {
            method: 'GET',
            headers:{
                "Authorization": `barear ${localStorage.getItem("token")}`
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log('User name ', data)
                this.setState({
                    user: data.users,
                    email: data.email
                    
                })
            
                const token = localStorage.getItem('token')
                //console.log(token)
                let base64Url = token.split('.')[1]
                //console.log('User aoth', base64Url)
                let base64 = base64Url.replace('-', '+').replace('_', '/')
                //console.log(base64)
                const userTok = JSON.parse(window.atob(base64))
                 console.log('Current user mail', userTok.email)

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                const currentUser = data.users.filter(user => {
                    if (user.email === userTok.email) {
                        this.setState({ user: user })
                        return user
                    }
                })
                 console.log('the current user is',currentUser)
            })
    }

    
    render() {
        return (
            <div>
                <div className="profile">
                    <div className="picSect">
                        <img className="profPic" src={require('../images/pizza.jpg')} alt="" />
                    </div>
                </div>
                <div className="userData">
                    <p>{this.state.user}</p>
                    <p></p>
                </div>
                {/* Here starts the recipes section */}
                <div className="card">
                    <div className="card-content">
                        <div className="media">
                            <div className="media-left">
                                <figure className="image is-128x128">
                                    <img src={require('../images/pizza.jpg')} alt="Placeholder image" />
                                </figure>
                            </div>
                            <div className="media-content">
                                <p className="title is-4">Homemade Piza</p>
                                <p className="subtitle is-5">Easy pizza and very tasty</p>
                            </div>
                            <div className="content-editor">
                                <i className="fas fa-trash-alt"></i>
                                <i className="far fa-edit"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-content">
                        <div className="media">
                            <div className="media-left">
                                <figure className="image is-128x128">
                                    <img src={require('../images/shrimp.jpg')} alt="Placeholder image" />
                                </figure>
                            </div>
                            <div className="media-content">
                                <p className="title is-4">Shrimp Stir With Egg Noodles</p>
                                <p className="subtitle is-5">Yummy shrimp easy and fast</p>
                            </div>
                            <div className="content-editor">
                                <i className="fas fa-trash-alt"></i>
                                <i className="far fa-edit"></i>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default withRouter(Profile);


