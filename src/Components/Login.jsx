import React, { Component } from 'react';
import { Form } from "react-bulma-components/full";
import { Link } from 'react-router-dom';


class Login extends Component {

    state = {
        error: {
            status: false,
            message: ""
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        const API_URL = "https://thawing-ocean-76607.herokuapp.com/api/v1"

        fetch(`${API_URL}/auth/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: e.target.email.value,
                password: e.target.password.value
            })
        })
            .then(response => response.json())
            .then(data => {
                if (typeof data.token !== "undefined") {
                    localStorage.setItem("token", data.token)
                } else {
                    this.setState({
                        error: {
                            status: true,
                            message: data.message
                        }
                    })
                }
                console.log('This is the data', data)
            })
            .catch(e => alert(e));
            this.props.history.push("/grenner");
    }


    render() {
        return (
            <React.Fragment>
                <div className="mainSection">
                    <form onSubmit={this.onSubmit} className="Logbox">
                        <div className="field">
                            <label className="label">Email</label>
                            <div className="control has-icons-left has-icons-right">
                                <input name="email" className="input " type="email" placeholder="Email@" />
                                <span className="icon is-small is-left">
                                    <i className="fas fa-envelope"></i>
                                </span>
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Password</label>
                            <div className="control">
                                <input name="password" className="input" type="password" placeholder="Password" />
                            </div>
                        </div>
                        {this.state.error.status && <p className="help is-danger">{this.state.error.message}</p>}
                        <input type="submit" className="button is-dark" value="Log In"/>
                        <div className="social">
                            {/*<Link type="submit" to="/grenner" className="button is-link"><i class="fab fa-facebook-f"></i>Facebook</Link>
                            <Link to="/grenner" className="button is-danger"><i class="fab fa-google"></i>Google</Link>*/}
                            {/* <Link to="/" class="button is-info"><i class="fab fa-twitter"></i>Twitter</Link> */}
                        </div>
                    </form>
                </div>

            </React.Fragment>

        );
    }
}

export default Login;

