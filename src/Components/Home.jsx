import React, { Component } from "react";
import {Link} from 'react-router-dom'

export default class Home extends Component {

  render() {
    return (
      <React.Fragment>
        <div className="willkommen">
          <div className="willkommen-section">
            <div>
              <img className="logo-home" src={require('../images/greener_logo.png')} alt="page logo"/>
            </div>
            <div className="register-section">
              <h1>Inicia Sesión</h1>
              <button className="homeLink">
                  <Link to="/grenner" className="is-text">Login</Link>
              </button>
              <h1>Registrate</h1>
                <button className="homeLink">
                  <Link  to="/signup" className="is-text">Signup</Link>
                </button> 
            </div>
          </div>
        </div>
          
      </React.Fragment>

    );
  }
}
