import React, { Component } from "react";

import { Link, withRouter } from "react-router-dom";
import Footer from "./Footer";

class Menu extends Component {
    handleLogout = () => {
        const { history } = this.props;

        localStorage.removeItem("token");
        history.push("/");
        return alert('goodbye :)')
    }

    state = { active: false }

    handleClick = () => {
        const { active } = this.state;
        this.setState({ active: !active });
    }

    render() {
        return (
            <React.Fragment>
                <nav role="navigation" aria-label="main navigation" className="navbar is-success" active={this.state.active}>
                    <div className="container fix-logo">
                        <div className="navbar-brand"  
                                active={this.state.active}
                                onClick={this.handleClick}>
                            <Link to="/" id="logo" className="navbar-brand">
                                <img src={require('../images/greener_logo.png')} alt="page logo"/>
                            </Link>
                            <span id="menu-toggle" role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample"
                               >
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>

                        </div>
                        <div id="navMenu" className="navbar-menu">
                            <div className="navbar-end">
                                <Link to="/grenner" className="navbar-item">Enter to Greener</Link>
                                <Link to="/profile" className="navbar-item"><i className="fas fa-user-circle"></i></Link>
                                <Link to="/foodmap">
                                    <img id="mapIcon" src={require('../images/cutlery.png')} alt="Icon to map" />
                                </Link>
                                <Link to="/" className="navbar-item" onClick={this.handleLogout}>Logout<i class="fas fa-sign-out-alt"></i></Link>
                            </div>
                        </div>
                    </div>
                </nav>
                <div className="divider is-success"></div>
                <Footer />
               
                
            </React.Fragment>

        );
    }
}

export default withRouter(Menu);


{/* <span><Link to="/grenner" className="navbar-item">Enter to Greener</Link></span>
<span><Link to="/login" className="navbar-item">Log in</Link></span>
<span><Link to="/signup" className="navbar-item">Sign up</Link></span>
<span><Link to="/home" className="navbar-item" onClick={this.handleLogout}>Logout</Link></span> */}