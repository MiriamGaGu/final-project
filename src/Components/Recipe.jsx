import React, { Component } from "react";
import { withRouter } from "react-router-dom";


class Recipe extends Component {


    render() {
        return (
            <div>
                {/* Here starts the recipes section */}
                <div className="card recipe-container">
                    <div className="card-image">
                        <figure className="image is-square">
                            <img src={require('../images/pizza.jpg')} alt="recipe" />
                        </figure>
                    </div>
                    <div className="media is-small recipe-instruc-cont">
                        <div>
                            <div>
                                <h1 className="title recipe-mainTitle">Homemade Piza</h1>
                            </div>
                            <div className="list-of-ingredients">
                                <h3 className="subtitle" >Ingredients</h3>
                                <ol  type="1">
                                    <li>1 package (1/4 ounce) active dry yeast</li>
                                    <li>1 teaspoon sugar</li>
                                    <li>1-1/4 cups warm water (110° to 115°)</li>
                                    <li>1/4 cup canola oil</li>
                                    <li>1 teaspoon salt</li>
                                    <li>3-1/2 cups all-purpose flour</li>
                                    <li>1/2 pound ground beef</li>
                                    <li>1 small onion, chopped</li>
                                    <li>1 can (15 ounces) tomato sauce</li>
                                    <li>3 teaspoons dried oregano</li>
                                    <li>1 teaspoon dried basil</li>
                                    <li>1 medium green pepper, diced</li>
                                    <li>2 cups shredded part-skim mozzarella cheese</li>
                                </ol>
                            </div>
                            <div className="list-of-directions">
                                <h3 className="subtitle">Directions</h3>
                                <ol type="1">
                                    <li>
                                        <p>In large bowl, dissolve yeast and sugar in water; let stand for 5 minutes. Add oil and salt. Stir in flour, a cup at a time, until a soft dough forms.</p>
                                    </li>
                                    <li>
                                        <p>Turn onto floured surface; knead until smooth and elastic, about 2-3 minutes. Place in a greased bowl, turning once to grease the top. Cover and let rise in a warm place until doubled, about 45 minutes. Meanwhile, cook beef and onion over medium heat until no longer pink; drain.</p>
                                    </li>
                                    <li>
                                        <p>Punch down dough; divide in half. Press each into a greased 12-in. pizza pan. Combine the tomato sauce, oregano and basil; spread over each crust. Top with beef mixture, green pepper and cheese.</p>
                                    </li>
                                    <li>
                                        <p>Bake at 400° for 25-30 minutes or until crust is lightly browned.</p>
                                    </li>
                                </ol>
                            </div>


                        </div>

                    </div>
                </div>
            </div >



        );
    }
}

export default withRouter(Recipe);


