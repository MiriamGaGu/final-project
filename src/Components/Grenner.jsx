import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import 'react-dropzone-uploader/dist/styles.css'
//import Dropzone from 'react-dropzone-uploader'
import Slide from "./Carousel";
// import MyUploader from './MyUploader'
const API_URL = 'https://thawing-ocean-76607.herokuapp.com/api/v1'

class Grenner extends Component {
    constructor() {
        super()
        this.state = {
            user: '',
            likes: '',
            title: '',
            description: '',
            photo: '',
            error: {
                status: false,
                message: ''
            }
        }
    }

    /*onChange = (e) =>{
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }*/

    componentDidMount() {
        fetch(`${API_URL}/users`, {
            method: 'GET',
            headers:{
                "Authorization": `barear ${localStorage.getItem("token")}`
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log('Are we log in?', data)
                this.setState({
                    user: data.users
                })

                const token = localStorage.getItem('token')
                //console.log(token)
                let base64Url = token.split('.')[1]
                //console.log('User aoth', base64Url)
                let base64 = base64Url.replace('-', '+').replace('_', '/')
                //console.log(base64)
                const userTok = JSON.parse(window.atob(base64))
                 console.log('Current user mail', userTok.email)

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                const currentUser = data.users.filter(user => {
                    if (user.email === userTok.email) {
                        this.setState({ user: user })
                        return user
                    }
                })
                 console.log('the current user is',currentUser)
            })
    }

    getRecipes = currentUser => {
         console.log(currentUser)
        const userId = currentUser[0]._id
        console.log(userId)
        
        fetch(`${API_URL}/recipes}`, {
            method: 'GET',
            headers:{
                "Authorization": `barear ${localStorage.getItem("token")}`
            }
        })
            .then(response => response.json())
            .then(data => {
                console.log('Should see recipes here',data)
                this.setState({
                    recipe: data.recipes
                })
            })
            .catch(e => alert(e))
    }

    onSubmit = e => {
        e.preventDefault()

        fetch(`${API_URL}/recipes`, {
            method: 'POST',
            headers: {
                "Authorization": `barear ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                //likes: e.target.likes.value,
                title: e.target.title.value,
                description: e.target.description.value,
                photo: e.target.photo.value,
                user: this.state.user._id
            })
        })
            .then(response => response.json())
            .then(data => {
                if (typeof data.token !== 'undefined') {

                    localStorage.setItem('token', data.token)
                    const url = window.decodeURIComponent(this.props.location.search)
                    console.log('these where you wanted to go', url)

                    if (url !== '') {
                        this.props.history.push('/' + url.split('/')[1] || '/')
                    } else {
                        this.props.history.push('/login')
                    }
                } else {
                    this.setState({
                        error: {
                            status: true,
                            message: data.message
                        }
                    })
                }
            })
            .catch(e => alert(e))
            this.props.history.push('/recipes')
            console.log('show me the recipe',this.props.history.push('/recipes'))

    }

    render() {
        return (
            <div>
                <Slide />
            
                <div className="field">
                    <div id="grenner-textarea" className="control">
                        <div className="share">
                            <h5>¡Comparte tus ideas con la comunidad!</h5>
                        </div>

                        <form onSubmit={this.onSubmit}>
                            <input  name="title" className="input is-primary home-title" type="text" placeholder="Título"/>
                            <textarea class="input" name="description" className="textarea is-primary" placeholder="Escribe tu receta aquí"></textarea>
                            <button value="submit" type="submit" name="publicar" className="button is-success publish green-button ">Compartir</button>
                            <button value="submit" type="submit" name="photo" className="button is-success green-button">
                                <i className="fas fa-camera-retro"></i>
                            </button>
                            {/* <MyUploader /> */}
                        </form>
                       
                    </div>
                </div>

               {/* {this.state.recipes.map(recipes => (*/}
                <form className="section">
                    <div className="card">
                        <div className="card-content green-card">
                             <div className="media">
                                <Link to="/recipe"><div className="feed-pic column">
                                        <figure className="image is-128x128">
                                           <img src="" alt="recipe" />
                                           {/*{recipes.photo}*/}
                                        </figure>
                                    </div>
                                </Link>
                                <div className="description-Title column">
                                    <p className="title is-4">{/*{recipes.title}*/}</p>
                                    <p className="subtitle is-5">{/*{recipes.description}*/}</p>
                                </div>

                                <div className="cards-editors">
                                    <i className="fas fa-trash-alt"></i>
                                    <i className="far fa-edit"></i>
                                    <i class="far fa-heart"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </form >
                {/*}))}*/}
            </div>



        );
    }
}

export default withRouter(Grenner);


// document.querySelector('a#open-modal').addEventListener('click', function(event) {
//   event.preventDefault();
//   var modal = document.querySelector('.modal');  // assuming you have only 1
//   var html = document.querySelector('html');
//   modal.classList.add('is-active');
//   html.classList.add('is-clipped');

//   modal.querySelector('.modal-background').addEventListener('click', function(e) {
//     e.preventDefault();
//     modal.classList.remove('is-active');
//     html.classList.remove('is-clipped');
//   });
// });

/**
 * class BulmaModal {
    constructor(selector) {
        this.elem = document.querySelector(selector)
        this.close_data()
    }

    show() {
        this.elem.classList.toggle('is-active')
        this.on_show()
    }

    close() {
        this.elem.classList.toggle('is-active')
        this.on_close()
    }

    close_data() {
        var modalClose = this.elem.querySelectorAll("[data-bulma-modal='close'],
        .modal-background")

        var that = this
        modalClose.forEach(function(e) {
            e.addEventListener("click", function() {

                that.elem.classList.toggle('is-active')

                var event = new Event('modal:close')

                that.elem.dispatchEvent(event);
            })
        })
    }

    on_show() {
        var event = new Event('modal:show')

        this.elem.dispatchEvent(event);
    }

    on_close() {
        var event = new Event('modal:close')

        this.elem.dispatchEvent(event);
    }

    addEventListener(event, callback) {
        this.elem.addEventListener(event, callback)
    }
}
 *
 */