import React, { Component } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';


class Slide extends Component {

    render() {
        return (
            <Carousel className="carousel" autoPlay="true" stopOnHover="true" dynamicHeight="true" emulateTouch="true" centerSlidePercentage="30">
                <div id="slide">
                    <img src={require("../assets/1.jpg")} alt="recipe"/>
                </div>
                <div id="slide">
                    <img src={require("../assets/2.jpg")} alt="recipe"/>
                </div>
                <div id="slide">
                    <img src={require("../assets/3.jpg")}alt="recipe" />
                    
                </div>
                <div id="slide">
                    <img src={require("../assets/4.jpg")} alt="recipe"/>
                </div>
            </Carousel>
        );
    }
}

export default Slide;