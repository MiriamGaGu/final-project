import React, { Component } from 'react';
import { Form } from "react-bulma-components/full";
import { Link } from 'react-router-dom';


class Signup extends Component {

    state = {
        error: {
            status: false,
            message: ''
        }
    }
    
    onSubmit = e => {
        e.preventDefault()

        const API_URL = 'http://thawing-ocean-76607.herokuapp.com/api/v1'

        fetch(`${API_URL}/auth/signup`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: e.target.name.value,
                email: e.target.email.value,
                password: e.target.password.value
            })
        })
            .then(response => response.json())
            .then(data => {
                if (typeof data.token !== "undefined") {
                    localStorage.setItem("token", data.token);
                    //const url = window.decodeURIComponent(this.props.location.search);
                    //console.log('The url from location', url)
                    //this.props.history.push("/" + url.split("/")[1]);
                } else {
                    this.setState({
                        error: {
                            status: true,
                            message: data.message
                        }
                    })
                }
                
                console.log('Data from sign up', data)
            })
            .catch(e => alert(e));
            this.props.history.push("/login");
    }

    render() {
        return (
            <React.Fragment>

                <div className="mainSection">
                    <form onSubmit={this.onSubmit} className="Logbox">
                        <div className="field">
                            <label className="label">Nombre</label>
                            <div className="control">
                                <input name="name" className="input" type="text" placeholder="Name" />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Email</label>
                            <div className="control has-icons-left has-icons-right">
                                <input name="email" className="input" type="email" placeholder="Email@" />
                                <span className="icon is-small is-left">
                                    <i className="fas fa-envelope"></i>
                                </span>
                            </div>
                            {/* <p className="help is-danger">This email is invalid</p> */}
                        </div>

                        <div className="field">
                            <label className="label">Password</label>
                            <div className="control">
                                <input name="password" className="input" type="password" placeholder="Password" />
                            </div>
                        </div>
                        {this.state.error.status && <p id="signupmsg" className="help is-success">{this.state.error.message}</p>}
                        <input type="submit" value="Signup" class="button is-dark" />
                       
                       
                        
                    </form>
                </div>
            </React.Fragment>

        );
    }
}

export default Signup;
